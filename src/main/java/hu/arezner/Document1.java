/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.arezner;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author attila rezner <attila.rezner@gmail.com>
 */
@WebService(serviceName = "Document1")
@Stateless()
public class Document1 {

    /**
     * Web service operation
     * @param requestXmlString
     * @return responseXmlString
     */
    @WebMethod(operationName = "getDocsToCustomer")
    public String getDocsToCustomer(@WebParam(name = "requestXmlString") String requestXmlString) {
        //TODO write your implementation code here:
        String responseXmlString;
        
        int customerId = getCustomerId(requestXmlString);
        responseXmlString = getDocsFromDoky(customerId);
        
        return responseXmlString;
    }
    
    private int getCustomerId(String xmlString) {
        System.out.println("request: " +xmlString);
        return 1;
    }
    
    private String getDocsFromDoky(int customerId) {
        return "documents of CustomerID: " +String.valueOf(customerId) +" from DOKY";
    }
    
}
